﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            var Name = " ";
            var age = 0;
            Console.WriteLine("please enter your name");
            Name = Console.ReadLine();
            Console.WriteLine("please enter your age");
            age = int.Parse(Console.ReadLine());
            Console.WriteLine("your name is " + Name + " and you are " + age + " years old. ");
            Console.WriteLine("your name is {0} and you are {1} years old.", Name, age);
            Console.WriteLine($"your name is {Name} and you are {age} years old.");
        }
    }
}
